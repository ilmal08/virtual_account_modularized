// ignore_for_file: public_member_api_docs, sort_constructors_first
// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/presentation/virtual_account_module_presentation.dart';

class VirtualAccountApp extends StatefulWidget {
  final String? tokken;
  const VirtualAccountApp({
    Key? key,
    this.tokken,
  }) : super(key: key);

  @override
  State<VirtualAccountApp> createState() => _VirtualAccountAppState();
}

class _VirtualAccountAppState extends State<VirtualAccountApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // you can initialize bloc here

    // return MultiProvider(
    //   // ignore: prefer_const_literals_to_create_immutables
    //   providers: [
    //     // you can pu provider inside here
    //   ],
    //   child: AppPresentation(),
    // );
    return VirtualAccountModulePresentation();
  }
}
