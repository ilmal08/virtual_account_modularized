import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:dio/dio.dart';
import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/config/url_api_string.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/local/hive_string_extension.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/model/failure_global_entitiy.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/model/favourite_billing_model/favorit_billing_freezed_model.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/repository/favourite_billing_repository/favourite_billing_repository.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/utils/dio_loggin_interceptor.dart';

class FavoritBillingService extends FavouriteBillingRepository {
  final Dio dio = Dio();
  FavoritBillingService() {
    dio.options.baseUrl = UriApi.baseApi;
    dio.interceptors.add(DioLogingIntercepotrs());
  }

  // var box = Hive.box(HiveLocalStorage.localStorage);

  @override
  Future<Either<Failure, FavouriteBillingFreezedModel>>
      getFavouriteBillingService({required String kodeTransaksi}) async {
    var box = Hive.box(HiveString.localStorage);
    late String ipAddress;
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      ipAddress = await Ipify.ipv4();
    } else if (connectivityResult == ConnectivityResult.wifi) {
      ipAddress = await Ipify.ipv4();
    }
    var body = {
      "kode_mitra": "${box.get(HiveString.kode_mitra_usr)}",
      "kode_cabang": "${box.get(HiveString.kode_cabang_usr)}",
      "kode_loket": "${box.get(HiveString.kode_loket_usr)}",
      "trxtype_id": '402',
      "browser_agent": "${box.get(HiveString.browserAgent)}",
      "ip_address": ipAddress,
      "id_api": "mobile",
      "ip_server": "68",
      "req_id": DateTime.now().millisecondsSinceEpoch.toString(),
      "session": "${box.get(HiveString.session_usr)}",
    };
    try {
      final response = await dio.post(
        UriApi.getFavouriteBilling,
        options: Options(
          headers: {
            'Content-Type': 'application/json',
          },
        ),
        data: body,
      );
      if (response.statusCode == 200) {
        return Right(FavouriteBillingFreezedModel.fromJson(response.data));
      } else {
        return Left(
          RequestFailure(
            code: response.statusCode ?? 500,
            message: (response.data["reason"] ??
                    'Something wrong, please try again.')
                .toString()
                .replaceFirst('[', '')
                .replaceAll(']', ''),
          ),
        );
      }
    } catch (e) {
      return Left(
        RequestFailure(
          code: 500,
          message: ('Something wrong, please try again.')
              .toString()
              .replaceFirst('[', '')
              .replaceAll(']', ''),
        ),
      );
    }
  }
}
