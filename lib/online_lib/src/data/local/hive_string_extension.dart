class HiveString {
  static const String localStorage = 'cache';
  // for hive box variable
  static const String authData = "auth_data";
  static const String ssn = "ssn";
  static const String browserAgent = "browser_agen";
  static const String session_usr = "session_usr";
  static const String user_name_usr = "user_name_usr";
  static const String nama_usr = "nama_usr";
  static const String nama_usaha_usr = "nama_usaha_usr";
  static const String account_num_usr = "account_num_usr";
  static const String kode_mitra_usr = "kode_mitra_usr";
  static const String kode_cabang_usr = "kode_cabang_usr";
  static const String kode_loket_usr = "kode_loket_usr";
  static const String alamat_jalan_usr = "alamat_jalan_usr";
  static const String alamat_perum_usr = "alamat_perum_usr";
  static const String alamat_kelurahan_usr = "alamat_kelurahan_usr";
  static const String alamat_kecamatan_usr = "alamat_kecamatan_usr";
  static const String alamat_kabupaten_usr = "alamat_kabupaten_usr";
  static const String alamat_provinsi_usr = "alamat_provinsi_usr";
  static const String email_usr = "email_usr";
  static const String no_hp_usr = "no_hp";
  static const String no_telepon_usr = "no_telepon_usr";
  static const String kelompok_id_usr = "kelompok_id_usr";
  static const String no_ktp_usr = "no_ktp";
  static const String agen_lpg_usr = "agen_lpg";
  static const String menu_favorit_usr = "menu_favorit_usr";
}
