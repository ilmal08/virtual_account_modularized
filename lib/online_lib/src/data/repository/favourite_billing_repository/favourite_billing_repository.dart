import 'package:dartz/dartz.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/model/failure_global_entitiy.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/model/favourite_billing_model/favorit_billing_freezed_model.dart';

abstract class FavouriteBillingRepository {
  Future<Either<Failure, FavouriteBillingFreezedModel>>
      getFavouriteBillingService({
    required String kodeTransaksi,
  });
}
