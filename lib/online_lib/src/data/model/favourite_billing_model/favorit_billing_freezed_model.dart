// To parse this JSON data, do
//
//     final favouriteBillingFreezedModel = favouriteBillingFreezedModelFromJson(jsonString);

import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'favorit_billing_freezed_model.freezed.dart';
part 'favorit_billing_freezed_model.g.dart';

@freezed
abstract class FavouriteBillingFreezedModel
    with _$FavouriteBillingFreezedModel {
  const factory FavouriteBillingFreezedModel({
    String? code,
    List<Datum>? data,
  }) = _FavouriteBillingFreezedModel;

  factory FavouriteBillingFreezedModel.fromJson(Map<String, dynamic> json) =>
      _$FavouriteBillingFreezedModelFromJson(json);
}

@freezed
abstract class Datum with _$Datum {
  const factory Datum({
    String? favorite_name,
    String? favorite_billing,
  }) = _Datum;

  factory Datum.fromJson(Map<String, dynamic> json) => _$DatumFromJson(json);
}
