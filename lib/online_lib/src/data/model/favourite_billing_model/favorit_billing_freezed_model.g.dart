// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorit_billing_freezed_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FavouriteBillingFreezedModel _$$_FavouriteBillingFreezedModelFromJson(
        Map<String, dynamic> json) =>
    _$_FavouriteBillingFreezedModel(
      code: json['code'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => Datum.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_FavouriteBillingFreezedModelToJson(
        _$_FavouriteBillingFreezedModel instance) =>
    <String, dynamic>{
      'code': instance.code,
      'data': instance.data,
    };

_$_Datum _$$_DatumFromJson(Map<String, dynamic> json) => _$_Datum(
      favorite_name: json['favorite_name'] as String?,
      favorite_billing: json['favorite_billing'] as String?,
    );

Map<String, dynamic> _$$_DatumToJson(_$_Datum instance) => <String, dynamic>{
      'favorite_name': instance.favorite_name,
      'favorite_billing': instance.favorite_billing,
    };
