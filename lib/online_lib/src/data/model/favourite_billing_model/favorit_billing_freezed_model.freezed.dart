// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'favorit_billing_freezed_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FavouriteBillingFreezedModel _$FavouriteBillingFreezedModelFromJson(
    Map<String, dynamic> json) {
  return _FavouriteBillingFreezedModel.fromJson(json);
}

/// @nodoc
mixin _$FavouriteBillingFreezedModel {
  String? get code => throw _privateConstructorUsedError;
  List<Datum>? get data => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FavouriteBillingFreezedModelCopyWith<FavouriteBillingFreezedModel>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FavouriteBillingFreezedModelCopyWith<$Res> {
  factory $FavouriteBillingFreezedModelCopyWith(
          FavouriteBillingFreezedModel value,
          $Res Function(FavouriteBillingFreezedModel) then) =
      _$FavouriteBillingFreezedModelCopyWithImpl<$Res>;
  $Res call({String? code, List<Datum>? data});
}

/// @nodoc
class _$FavouriteBillingFreezedModelCopyWithImpl<$Res>
    implements $FavouriteBillingFreezedModelCopyWith<$Res> {
  _$FavouriteBillingFreezedModelCopyWithImpl(this._value, this._then);

  final FavouriteBillingFreezedModel _value;
  // ignore: unused_field
  final $Res Function(FavouriteBillingFreezedModel) _then;

  @override
  $Res call({
    Object? code = freezed,
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      data: data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as List<Datum>?,
    ));
  }
}

/// @nodoc
abstract class _$$_FavouriteBillingFreezedModelCopyWith<$Res>
    implements $FavouriteBillingFreezedModelCopyWith<$Res> {
  factory _$$_FavouriteBillingFreezedModelCopyWith(
          _$_FavouriteBillingFreezedModel value,
          $Res Function(_$_FavouriteBillingFreezedModel) then) =
      __$$_FavouriteBillingFreezedModelCopyWithImpl<$Res>;
  @override
  $Res call({String? code, List<Datum>? data});
}

/// @nodoc
class __$$_FavouriteBillingFreezedModelCopyWithImpl<$Res>
    extends _$FavouriteBillingFreezedModelCopyWithImpl<$Res>
    implements _$$_FavouriteBillingFreezedModelCopyWith<$Res> {
  __$$_FavouriteBillingFreezedModelCopyWithImpl(
      _$_FavouriteBillingFreezedModel _value,
      $Res Function(_$_FavouriteBillingFreezedModel) _then)
      : super(_value, (v) => _then(v as _$_FavouriteBillingFreezedModel));

  @override
  _$_FavouriteBillingFreezedModel get _value =>
      super._value as _$_FavouriteBillingFreezedModel;

  @override
  $Res call({
    Object? code = freezed,
    Object? data = freezed,
  }) {
    return _then(_$_FavouriteBillingFreezedModel(
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      data: data == freezed
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<Datum>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_FavouriteBillingFreezedModel implements _FavouriteBillingFreezedModel {
  const _$_FavouriteBillingFreezedModel({this.code, final List<Datum>? data})
      : _data = data;

  factory _$_FavouriteBillingFreezedModel.fromJson(Map<String, dynamic> json) =>
      _$$_FavouriteBillingFreezedModelFromJson(json);

  @override
  final String? code;
  final List<Datum>? _data;
  @override
  List<Datum>? get data {
    final value = _data;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'FavouriteBillingFreezedModel(code: $code, data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FavouriteBillingFreezedModel &&
            const DeepCollectionEquality().equals(other.code, code) &&
            const DeepCollectionEquality().equals(other._data, _data));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(code),
      const DeepCollectionEquality().hash(_data));

  @JsonKey(ignore: true)
  @override
  _$$_FavouriteBillingFreezedModelCopyWith<_$_FavouriteBillingFreezedModel>
      get copyWith => __$$_FavouriteBillingFreezedModelCopyWithImpl<
          _$_FavouriteBillingFreezedModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FavouriteBillingFreezedModelToJson(
      this,
    );
  }
}

abstract class _FavouriteBillingFreezedModel
    implements FavouriteBillingFreezedModel {
  const factory _FavouriteBillingFreezedModel(
      {final String? code,
      final List<Datum>? data}) = _$_FavouriteBillingFreezedModel;

  factory _FavouriteBillingFreezedModel.fromJson(Map<String, dynamic> json) =
      _$_FavouriteBillingFreezedModel.fromJson;

  @override
  String? get code;
  @override
  List<Datum>? get data;
  @override
  @JsonKey(ignore: true)
  _$$_FavouriteBillingFreezedModelCopyWith<_$_FavouriteBillingFreezedModel>
      get copyWith => throw _privateConstructorUsedError;
}

Datum _$DatumFromJson(Map<String, dynamic> json) {
  return _Datum.fromJson(json);
}

/// @nodoc
mixin _$Datum {
  String? get favorite_name => throw _privateConstructorUsedError;
  String? get favorite_billing => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DatumCopyWith<Datum> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DatumCopyWith<$Res> {
  factory $DatumCopyWith(Datum value, $Res Function(Datum) then) =
      _$DatumCopyWithImpl<$Res>;
  $Res call({String? favorite_name, String? favorite_billing});
}

/// @nodoc
class _$DatumCopyWithImpl<$Res> implements $DatumCopyWith<$Res> {
  _$DatumCopyWithImpl(this._value, this._then);

  final Datum _value;
  // ignore: unused_field
  final $Res Function(Datum) _then;

  @override
  $Res call({
    Object? favorite_name = freezed,
    Object? favorite_billing = freezed,
  }) {
    return _then(_value.copyWith(
      favorite_name: favorite_name == freezed
          ? _value.favorite_name
          : favorite_name // ignore: cast_nullable_to_non_nullable
              as String?,
      favorite_billing: favorite_billing == freezed
          ? _value.favorite_billing
          : favorite_billing // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_DatumCopyWith<$Res> implements $DatumCopyWith<$Res> {
  factory _$$_DatumCopyWith(_$_Datum value, $Res Function(_$_Datum) then) =
      __$$_DatumCopyWithImpl<$Res>;
  @override
  $Res call({String? favorite_name, String? favorite_billing});
}

/// @nodoc
class __$$_DatumCopyWithImpl<$Res> extends _$DatumCopyWithImpl<$Res>
    implements _$$_DatumCopyWith<$Res> {
  __$$_DatumCopyWithImpl(_$_Datum _value, $Res Function(_$_Datum) _then)
      : super(_value, (v) => _then(v as _$_Datum));

  @override
  _$_Datum get _value => super._value as _$_Datum;

  @override
  $Res call({
    Object? favorite_name = freezed,
    Object? favorite_billing = freezed,
  }) {
    return _then(_$_Datum(
      favorite_name: favorite_name == freezed
          ? _value.favorite_name
          : favorite_name // ignore: cast_nullable_to_non_nullable
              as String?,
      favorite_billing: favorite_billing == freezed
          ? _value.favorite_billing
          : favorite_billing // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Datum implements _Datum {
  const _$_Datum({this.favorite_name, this.favorite_billing});

  factory _$_Datum.fromJson(Map<String, dynamic> json) =>
      _$$_DatumFromJson(json);

  @override
  final String? favorite_name;
  @override
  final String? favorite_billing;

  @override
  String toString() {
    return 'Datum(favorite_name: $favorite_name, favorite_billing: $favorite_billing)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Datum &&
            const DeepCollectionEquality()
                .equals(other.favorite_name, favorite_name) &&
            const DeepCollectionEquality()
                .equals(other.favorite_billing, favorite_billing));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(favorite_name),
      const DeepCollectionEquality().hash(favorite_billing));

  @JsonKey(ignore: true)
  @override
  _$$_DatumCopyWith<_$_Datum> get copyWith =>
      __$$_DatumCopyWithImpl<_$_Datum>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DatumToJson(
      this,
    );
  }
}

abstract class _Datum implements Datum {
  const factory _Datum(
      {final String? favorite_name, final String? favorite_billing}) = _$_Datum;

  factory _Datum.fromJson(Map<String, dynamic> json) = _$_Datum.fromJson;

  @override
  String? get favorite_name;
  @override
  String? get favorite_billing;
  @override
  @JsonKey(ignore: true)
  _$$_DatumCopyWith<_$_Datum> get copyWith =>
      throw _privateConstructorUsedError;
}
