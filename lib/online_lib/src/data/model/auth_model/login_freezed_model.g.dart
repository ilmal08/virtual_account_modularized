// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_freezed_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LoginFreezedModel _$$_LoginFreezedModelFromJson(Map<String, dynamic> json) =>
    _$_LoginFreezedModel(
      code: json['code'] as String?,
      sso: json['sso'] as bool?,
      session: json['session'] as String?,
      dataUser: json['dataUser'] == null
          ? null
          : DataUser.fromJson(json['dataUser'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_LoginFreezedModelToJson(
        _$_LoginFreezedModel instance) =>
    <String, dynamic>{
      'code': instance.code,
      'sso': instance.sso,
      'session': instance.session,
      'dataUser': instance.dataUser,
    };

_$_DataUser _$$_DataUserFromJson(Map<String, dynamic> json) => _$_DataUser(
      is_custom_ebk: json['is_custom_ebk'] as int?,
      klasifikasi_agen: json['klasifikasi_agen'] as String?,
      found: json['found'] as int?,
      kode_wil_bi: json['kode_wil_bi'] as String?,
      username: json['username'] as String?,
      ssn: json['ssn'] as String?,
      ssn_2: json['ssn_2'] as String?,
      status: json['status'] as String?,
      kode_mitra: json['kode_mitra'] as String?,
      last_login: json['last_login'] == null
          ? null
          : DateTime.parse(json['last_login'] as String),
      alamat_jalan: json['alamat_jalan'] as String?,
      alamat_rt_perum: json['alamat_rt_perum'] as String?,
      alamat_kelurahan: json['alamat_kelurahan'] as String?,
      alamat_kecamatan: json['alamat_kecamatan'] as String?,
      alamat_kabupaten_kota: json['alamat_kabupaten_kota'] as String?,
      alamat_propinsi: json['alamat_propinsi'] as String?,
      email: json['email'] as String?,
      no_tlp_hp: json['no_tlp_hp'] as String?,
      no_tlp_rmh: json['no_tlp_rmh'] as String?,
      agent_type: json['agent_type'] as String?,
      kode_loket: json['kode_loket'] as String?,
      nama: json['nama'] as String?,
      kode_cabang: json['kode_cabang'] as String?,
      priv_id: json['priv_id'] as String?,
      favourite: json['favourite'] as String?,
      favourite_mobile: (json['favourite_mobile'] as List<dynamic>?)
          ?.map((e) => FavouriteMobile.fromJson(e as Map<String, dynamic>))
          .toList(),
      accountNum: json['accountNum'] as String?,
      kelompok_id: json['kelompok_id'] as String?,
      menu: json['menu'],
      group_id: json['group_id'],
      status_mitra: json['status_mitra'],
      no_ktp: json['no_ktp'] as String?,
      agen_elpiji: json['agen_elpiji'],
      nama_usaha: json['nama_usaha'] as String?,
      flag_force_lokasi: json['flag_force_lokasi'] as int?,
    );

Map<String, dynamic> _$$_DataUserToJson(_$_DataUser instance) =>
    <String, dynamic>{
      'is_custom_ebk': instance.is_custom_ebk,
      'klasifikasi_agen': instance.klasifikasi_agen,
      'found': instance.found,
      'kode_wil_bi': instance.kode_wil_bi,
      'username': instance.username,
      'ssn': instance.ssn,
      'ssn_2': instance.ssn_2,
      'status': instance.status,
      'kode_mitra': instance.kode_mitra,
      'last_login': instance.last_login?.toIso8601String(),
      'alamat_jalan': instance.alamat_jalan,
      'alamat_rt_perum': instance.alamat_rt_perum,
      'alamat_kelurahan': instance.alamat_kelurahan,
      'alamat_kecamatan': instance.alamat_kecamatan,
      'alamat_kabupaten_kota': instance.alamat_kabupaten_kota,
      'alamat_propinsi': instance.alamat_propinsi,
      'email': instance.email,
      'no_tlp_hp': instance.no_tlp_hp,
      'no_tlp_rmh': instance.no_tlp_rmh,
      'agent_type': instance.agent_type,
      'kode_loket': instance.kode_loket,
      'nama': instance.nama,
      'kode_cabang': instance.kode_cabang,
      'priv_id': instance.priv_id,
      'favourite': instance.favourite,
      'favourite_mobile': instance.favourite_mobile,
      'accountNum': instance.accountNum,
      'kelompok_id': instance.kelompok_id,
      'menu': instance.menu,
      'group_id': instance.group_id,
      'status_mitra': instance.status_mitra,
      'no_ktp': instance.no_ktp,
      'agen_elpiji': instance.agen_elpiji,
      'nama_usaha': instance.nama_usaha,
      'flag_force_lokasi': instance.flag_force_lokasi,
    };

_$_FavouriteMobileSave _$$_FavouriteMobileSaveFromJson(
        Map<String, dynamic> json) =>
    _$_FavouriteMobileSave(
      namaMenu: json['namaMenu'] as String?,
      id: json['id'] as String?,
    );

Map<String, dynamic> _$$_FavouriteMobileSaveToJson(
        _$_FavouriteMobileSave instance) =>
    <String, dynamic>{
      'namaMenu': instance.namaMenu,
      'id': instance.id,
    };

_$_FavouriteMobile _$$_FavouriteMobileFromJson(Map<String, dynamic> json) =>
    _$_FavouriteMobile(
      type: json['type'] as String?,
      namaMenu: json['namaMenu'] as String?,
      id: json['id'] as String?,
      namaIcon: json['namaIcon'] as String?,
      route: json['route'] as String?,
    );

Map<String, dynamic> _$$_FavouriteMobileToJson(_$_FavouriteMobile instance) =>
    <String, dynamic>{
      'type': instance.type,
      'namaMenu': instance.namaMenu,
      'id': instance.id,
      'namaIcon': instance.namaIcon,
      'route': instance.route,
    };
