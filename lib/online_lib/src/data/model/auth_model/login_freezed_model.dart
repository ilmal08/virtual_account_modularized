// To parse this JSON data, do
//
//     final loginFreezedModel = loginFreezedModelFromJson(jsonString);

// ignore_for_file: non_constant_identifier_names

import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'login_freezed_model.freezed.dart';
part 'login_freezed_model.g.dart';

@freezed
abstract class LoginFreezedModel with _$LoginFreezedModel {
  const factory LoginFreezedModel({
    String? code,
    bool? sso,
    String? session,
    DataUser? dataUser,
  }) = _LoginFreezedModel;

  factory LoginFreezedModel.fromJson(Map<String, dynamic> json) =>
      _$LoginFreezedModelFromJson(json);
}

@freezed
abstract class DataUser with _$DataUser {
  const factory DataUser({
    int? is_custom_ebk,
    String? klasifikasi_agen,
    int? found,
    String? kode_wil_bi,
    String? username,
    String? ssn,
    String? ssn_2,
    String? status,
    String? kode_mitra,
    DateTime? last_login,
    String? alamat_jalan,
    String? alamat_rt_perum,
    String? alamat_kelurahan,
    String? alamat_kecamatan,
    String? alamat_kabupaten_kota,
    String? alamat_propinsi,
    String? email,
    String? no_tlp_hp,
    String? no_tlp_rmh,
    String? agent_type,
    String? kode_loket,
    String? nama,
    String? kode_cabang,
    String? priv_id,
    String? favourite,
    List<FavouriteMobile>? favourite_mobile,
    String? accountNum,
    String? kelompok_id,
    dynamic menu,
    dynamic group_id,
    dynamic status_mitra,
    String? no_ktp,
    dynamic agen_elpiji,
    String? nama_usaha,
    int? flag_force_lokasi,
  }) = _DataUser;

  factory DataUser.fromJson(Map<String, dynamic> json) =>
      _$DataUserFromJson(json);
}

@freezed
abstract class FavouriteMobileSave with _$FavouriteMobileSave {
  const factory FavouriteMobileSave({
    String? namaMenu,
    String? id,
  }) = _FavouriteMobileSave;

  factory FavouriteMobileSave.fromJson(Map<String, dynamic> json) =>
      _$FavouriteMobileSaveFromJson(json);
}

@freezed
abstract class FavouriteMobile with _$FavouriteMobile {
  const factory FavouriteMobile({
    String? type,
    String? namaMenu,
    String? id,
    String? namaIcon,
    String? route,
  }) = _FavouriteMobile;

  factory FavouriteMobile.fromJson(Map<String, dynamic> json) =>
      _$FavouriteMobileFromJson(json);
}
