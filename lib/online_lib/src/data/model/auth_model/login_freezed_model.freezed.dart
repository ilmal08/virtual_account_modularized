// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'login_freezed_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LoginFreezedModel _$LoginFreezedModelFromJson(Map<String, dynamic> json) {
  return _LoginFreezedModel.fromJson(json);
}

/// @nodoc
mixin _$LoginFreezedModel {
  String? get code => throw _privateConstructorUsedError;
  bool? get sso => throw _privateConstructorUsedError;
  String? get session => throw _privateConstructorUsedError;
  DataUser? get dataUser => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginFreezedModelCopyWith<LoginFreezedModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginFreezedModelCopyWith<$Res> {
  factory $LoginFreezedModelCopyWith(
          LoginFreezedModel value, $Res Function(LoginFreezedModel) then) =
      _$LoginFreezedModelCopyWithImpl<$Res>;
  $Res call({String? code, bool? sso, String? session, DataUser? dataUser});

  $DataUserCopyWith<$Res>? get dataUser;
}

/// @nodoc
class _$LoginFreezedModelCopyWithImpl<$Res>
    implements $LoginFreezedModelCopyWith<$Res> {
  _$LoginFreezedModelCopyWithImpl(this._value, this._then);

  final LoginFreezedModel _value;
  // ignore: unused_field
  final $Res Function(LoginFreezedModel) _then;

  @override
  $Res call({
    Object? code = freezed,
    Object? sso = freezed,
    Object? session = freezed,
    Object? dataUser = freezed,
  }) {
    return _then(_value.copyWith(
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      sso: sso == freezed
          ? _value.sso
          : sso // ignore: cast_nullable_to_non_nullable
              as bool?,
      session: session == freezed
          ? _value.session
          : session // ignore: cast_nullable_to_non_nullable
              as String?,
      dataUser: dataUser == freezed
          ? _value.dataUser
          : dataUser // ignore: cast_nullable_to_non_nullable
              as DataUser?,
    ));
  }

  @override
  $DataUserCopyWith<$Res>? get dataUser {
    if (_value.dataUser == null) {
      return null;
    }

    return $DataUserCopyWith<$Res>(_value.dataUser!, (value) {
      return _then(_value.copyWith(dataUser: value));
    });
  }
}

/// @nodoc
abstract class _$$_LoginFreezedModelCopyWith<$Res>
    implements $LoginFreezedModelCopyWith<$Res> {
  factory _$$_LoginFreezedModelCopyWith(_$_LoginFreezedModel value,
          $Res Function(_$_LoginFreezedModel) then) =
      __$$_LoginFreezedModelCopyWithImpl<$Res>;
  @override
  $Res call({String? code, bool? sso, String? session, DataUser? dataUser});

  @override
  $DataUserCopyWith<$Res>? get dataUser;
}

/// @nodoc
class __$$_LoginFreezedModelCopyWithImpl<$Res>
    extends _$LoginFreezedModelCopyWithImpl<$Res>
    implements _$$_LoginFreezedModelCopyWith<$Res> {
  __$$_LoginFreezedModelCopyWithImpl(
      _$_LoginFreezedModel _value, $Res Function(_$_LoginFreezedModel) _then)
      : super(_value, (v) => _then(v as _$_LoginFreezedModel));

  @override
  _$_LoginFreezedModel get _value => super._value as _$_LoginFreezedModel;

  @override
  $Res call({
    Object? code = freezed,
    Object? sso = freezed,
    Object? session = freezed,
    Object? dataUser = freezed,
  }) {
    return _then(_$_LoginFreezedModel(
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      sso: sso == freezed
          ? _value.sso
          : sso // ignore: cast_nullable_to_non_nullable
              as bool?,
      session: session == freezed
          ? _value.session
          : session // ignore: cast_nullable_to_non_nullable
              as String?,
      dataUser: dataUser == freezed
          ? _value.dataUser
          : dataUser // ignore: cast_nullable_to_non_nullable
              as DataUser?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LoginFreezedModel implements _LoginFreezedModel {
  const _$_LoginFreezedModel(
      {this.code, this.sso, this.session, this.dataUser});

  factory _$_LoginFreezedModel.fromJson(Map<String, dynamic> json) =>
      _$$_LoginFreezedModelFromJson(json);

  @override
  final String? code;
  @override
  final bool? sso;
  @override
  final String? session;
  @override
  final DataUser? dataUser;

  @override
  String toString() {
    return 'LoginFreezedModel(code: $code, sso: $sso, session: $session, dataUser: $dataUser)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LoginFreezedModel &&
            const DeepCollectionEquality().equals(other.code, code) &&
            const DeepCollectionEquality().equals(other.sso, sso) &&
            const DeepCollectionEquality().equals(other.session, session) &&
            const DeepCollectionEquality().equals(other.dataUser, dataUser));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(code),
      const DeepCollectionEquality().hash(sso),
      const DeepCollectionEquality().hash(session),
      const DeepCollectionEquality().hash(dataUser));

  @JsonKey(ignore: true)
  @override
  _$$_LoginFreezedModelCopyWith<_$_LoginFreezedModel> get copyWith =>
      __$$_LoginFreezedModelCopyWithImpl<_$_LoginFreezedModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LoginFreezedModelToJson(
      this,
    );
  }
}

abstract class _LoginFreezedModel implements LoginFreezedModel {
  const factory _LoginFreezedModel(
      {final String? code,
      final bool? sso,
      final String? session,
      final DataUser? dataUser}) = _$_LoginFreezedModel;

  factory _LoginFreezedModel.fromJson(Map<String, dynamic> json) =
      _$_LoginFreezedModel.fromJson;

  @override
  String? get code;
  @override
  bool? get sso;
  @override
  String? get session;
  @override
  DataUser? get dataUser;
  @override
  @JsonKey(ignore: true)
  _$$_LoginFreezedModelCopyWith<_$_LoginFreezedModel> get copyWith =>
      throw _privateConstructorUsedError;
}

DataUser _$DataUserFromJson(Map<String, dynamic> json) {
  return _DataUser.fromJson(json);
}

/// @nodoc
mixin _$DataUser {
  int? get is_custom_ebk => throw _privateConstructorUsedError;
  String? get klasifikasi_agen => throw _privateConstructorUsedError;
  int? get found => throw _privateConstructorUsedError;
  String? get kode_wil_bi => throw _privateConstructorUsedError;
  String? get username => throw _privateConstructorUsedError;
  String? get ssn => throw _privateConstructorUsedError;
  String? get ssn_2 => throw _privateConstructorUsedError;
  String? get status => throw _privateConstructorUsedError;
  String? get kode_mitra => throw _privateConstructorUsedError;
  DateTime? get last_login => throw _privateConstructorUsedError;
  String? get alamat_jalan => throw _privateConstructorUsedError;
  String? get alamat_rt_perum => throw _privateConstructorUsedError;
  String? get alamat_kelurahan => throw _privateConstructorUsedError;
  String? get alamat_kecamatan => throw _privateConstructorUsedError;
  String? get alamat_kabupaten_kota => throw _privateConstructorUsedError;
  String? get alamat_propinsi => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get no_tlp_hp => throw _privateConstructorUsedError;
  String? get no_tlp_rmh => throw _privateConstructorUsedError;
  String? get agent_type => throw _privateConstructorUsedError;
  String? get kode_loket => throw _privateConstructorUsedError;
  String? get nama => throw _privateConstructorUsedError;
  String? get kode_cabang => throw _privateConstructorUsedError;
  String? get priv_id => throw _privateConstructorUsedError;
  String? get favourite => throw _privateConstructorUsedError;
  List<FavouriteMobile>? get favourite_mobile =>
      throw _privateConstructorUsedError;
  String? get accountNum => throw _privateConstructorUsedError;
  String? get kelompok_id => throw _privateConstructorUsedError;
  dynamic get menu => throw _privateConstructorUsedError;
  dynamic get group_id => throw _privateConstructorUsedError;
  dynamic get status_mitra => throw _privateConstructorUsedError;
  String? get no_ktp => throw _privateConstructorUsedError;
  dynamic get agen_elpiji => throw _privateConstructorUsedError;
  String? get nama_usaha => throw _privateConstructorUsedError;
  int? get flag_force_lokasi => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DataUserCopyWith<DataUser> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DataUserCopyWith<$Res> {
  factory $DataUserCopyWith(DataUser value, $Res Function(DataUser) then) =
      _$DataUserCopyWithImpl<$Res>;
  $Res call(
      {int? is_custom_ebk,
      String? klasifikasi_agen,
      int? found,
      String? kode_wil_bi,
      String? username,
      String? ssn,
      String? ssn_2,
      String? status,
      String? kode_mitra,
      DateTime? last_login,
      String? alamat_jalan,
      String? alamat_rt_perum,
      String? alamat_kelurahan,
      String? alamat_kecamatan,
      String? alamat_kabupaten_kota,
      String? alamat_propinsi,
      String? email,
      String? no_tlp_hp,
      String? no_tlp_rmh,
      String? agent_type,
      String? kode_loket,
      String? nama,
      String? kode_cabang,
      String? priv_id,
      String? favourite,
      List<FavouriteMobile>? favourite_mobile,
      String? accountNum,
      String? kelompok_id,
      dynamic menu,
      dynamic group_id,
      dynamic status_mitra,
      String? no_ktp,
      dynamic agen_elpiji,
      String? nama_usaha,
      int? flag_force_lokasi});
}

/// @nodoc
class _$DataUserCopyWithImpl<$Res> implements $DataUserCopyWith<$Res> {
  _$DataUserCopyWithImpl(this._value, this._then);

  final DataUser _value;
  // ignore: unused_field
  final $Res Function(DataUser) _then;

  @override
  $Res call({
    Object? is_custom_ebk = freezed,
    Object? klasifikasi_agen = freezed,
    Object? found = freezed,
    Object? kode_wil_bi = freezed,
    Object? username = freezed,
    Object? ssn = freezed,
    Object? ssn_2 = freezed,
    Object? status = freezed,
    Object? kode_mitra = freezed,
    Object? last_login = freezed,
    Object? alamat_jalan = freezed,
    Object? alamat_rt_perum = freezed,
    Object? alamat_kelurahan = freezed,
    Object? alamat_kecamatan = freezed,
    Object? alamat_kabupaten_kota = freezed,
    Object? alamat_propinsi = freezed,
    Object? email = freezed,
    Object? no_tlp_hp = freezed,
    Object? no_tlp_rmh = freezed,
    Object? agent_type = freezed,
    Object? kode_loket = freezed,
    Object? nama = freezed,
    Object? kode_cabang = freezed,
    Object? priv_id = freezed,
    Object? favourite = freezed,
    Object? favourite_mobile = freezed,
    Object? accountNum = freezed,
    Object? kelompok_id = freezed,
    Object? menu = freezed,
    Object? group_id = freezed,
    Object? status_mitra = freezed,
    Object? no_ktp = freezed,
    Object? agen_elpiji = freezed,
    Object? nama_usaha = freezed,
    Object? flag_force_lokasi = freezed,
  }) {
    return _then(_value.copyWith(
      is_custom_ebk: is_custom_ebk == freezed
          ? _value.is_custom_ebk
          : is_custom_ebk // ignore: cast_nullable_to_non_nullable
              as int?,
      klasifikasi_agen: klasifikasi_agen == freezed
          ? _value.klasifikasi_agen
          : klasifikasi_agen // ignore: cast_nullable_to_non_nullable
              as String?,
      found: found == freezed
          ? _value.found
          : found // ignore: cast_nullable_to_non_nullable
              as int?,
      kode_wil_bi: kode_wil_bi == freezed
          ? _value.kode_wil_bi
          : kode_wil_bi // ignore: cast_nullable_to_non_nullable
              as String?,
      username: username == freezed
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      ssn: ssn == freezed
          ? _value.ssn
          : ssn // ignore: cast_nullable_to_non_nullable
              as String?,
      ssn_2: ssn_2 == freezed
          ? _value.ssn_2
          : ssn_2 // ignore: cast_nullable_to_non_nullable
              as String?,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      kode_mitra: kode_mitra == freezed
          ? _value.kode_mitra
          : kode_mitra // ignore: cast_nullable_to_non_nullable
              as String?,
      last_login: last_login == freezed
          ? _value.last_login
          : last_login // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      alamat_jalan: alamat_jalan == freezed
          ? _value.alamat_jalan
          : alamat_jalan // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat_rt_perum: alamat_rt_perum == freezed
          ? _value.alamat_rt_perum
          : alamat_rt_perum // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat_kelurahan: alamat_kelurahan == freezed
          ? _value.alamat_kelurahan
          : alamat_kelurahan // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat_kecamatan: alamat_kecamatan == freezed
          ? _value.alamat_kecamatan
          : alamat_kecamatan // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat_kabupaten_kota: alamat_kabupaten_kota == freezed
          ? _value.alamat_kabupaten_kota
          : alamat_kabupaten_kota // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat_propinsi: alamat_propinsi == freezed
          ? _value.alamat_propinsi
          : alamat_propinsi // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      no_tlp_hp: no_tlp_hp == freezed
          ? _value.no_tlp_hp
          : no_tlp_hp // ignore: cast_nullable_to_non_nullable
              as String?,
      no_tlp_rmh: no_tlp_rmh == freezed
          ? _value.no_tlp_rmh
          : no_tlp_rmh // ignore: cast_nullable_to_non_nullable
              as String?,
      agent_type: agent_type == freezed
          ? _value.agent_type
          : agent_type // ignore: cast_nullable_to_non_nullable
              as String?,
      kode_loket: kode_loket == freezed
          ? _value.kode_loket
          : kode_loket // ignore: cast_nullable_to_non_nullable
              as String?,
      nama: nama == freezed
          ? _value.nama
          : nama // ignore: cast_nullable_to_non_nullable
              as String?,
      kode_cabang: kode_cabang == freezed
          ? _value.kode_cabang
          : kode_cabang // ignore: cast_nullable_to_non_nullable
              as String?,
      priv_id: priv_id == freezed
          ? _value.priv_id
          : priv_id // ignore: cast_nullable_to_non_nullable
              as String?,
      favourite: favourite == freezed
          ? _value.favourite
          : favourite // ignore: cast_nullable_to_non_nullable
              as String?,
      favourite_mobile: favourite_mobile == freezed
          ? _value.favourite_mobile
          : favourite_mobile // ignore: cast_nullable_to_non_nullable
              as List<FavouriteMobile>?,
      accountNum: accountNum == freezed
          ? _value.accountNum
          : accountNum // ignore: cast_nullable_to_non_nullable
              as String?,
      kelompok_id: kelompok_id == freezed
          ? _value.kelompok_id
          : kelompok_id // ignore: cast_nullable_to_non_nullable
              as String?,
      menu: menu == freezed
          ? _value.menu
          : menu // ignore: cast_nullable_to_non_nullable
              as dynamic,
      group_id: group_id == freezed
          ? _value.group_id
          : group_id // ignore: cast_nullable_to_non_nullable
              as dynamic,
      status_mitra: status_mitra == freezed
          ? _value.status_mitra
          : status_mitra // ignore: cast_nullable_to_non_nullable
              as dynamic,
      no_ktp: no_ktp == freezed
          ? _value.no_ktp
          : no_ktp // ignore: cast_nullable_to_non_nullable
              as String?,
      agen_elpiji: agen_elpiji == freezed
          ? _value.agen_elpiji
          : agen_elpiji // ignore: cast_nullable_to_non_nullable
              as dynamic,
      nama_usaha: nama_usaha == freezed
          ? _value.nama_usaha
          : nama_usaha // ignore: cast_nullable_to_non_nullable
              as String?,
      flag_force_lokasi: flag_force_lokasi == freezed
          ? _value.flag_force_lokasi
          : flag_force_lokasi // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
abstract class _$$_DataUserCopyWith<$Res> implements $DataUserCopyWith<$Res> {
  factory _$$_DataUserCopyWith(
          _$_DataUser value, $Res Function(_$_DataUser) then) =
      __$$_DataUserCopyWithImpl<$Res>;
  @override
  $Res call(
      {int? is_custom_ebk,
      String? klasifikasi_agen,
      int? found,
      String? kode_wil_bi,
      String? username,
      String? ssn,
      String? ssn_2,
      String? status,
      String? kode_mitra,
      DateTime? last_login,
      String? alamat_jalan,
      String? alamat_rt_perum,
      String? alamat_kelurahan,
      String? alamat_kecamatan,
      String? alamat_kabupaten_kota,
      String? alamat_propinsi,
      String? email,
      String? no_tlp_hp,
      String? no_tlp_rmh,
      String? agent_type,
      String? kode_loket,
      String? nama,
      String? kode_cabang,
      String? priv_id,
      String? favourite,
      List<FavouriteMobile>? favourite_mobile,
      String? accountNum,
      String? kelompok_id,
      dynamic menu,
      dynamic group_id,
      dynamic status_mitra,
      String? no_ktp,
      dynamic agen_elpiji,
      String? nama_usaha,
      int? flag_force_lokasi});
}

/// @nodoc
class __$$_DataUserCopyWithImpl<$Res> extends _$DataUserCopyWithImpl<$Res>
    implements _$$_DataUserCopyWith<$Res> {
  __$$_DataUserCopyWithImpl(
      _$_DataUser _value, $Res Function(_$_DataUser) _then)
      : super(_value, (v) => _then(v as _$_DataUser));

  @override
  _$_DataUser get _value => super._value as _$_DataUser;

  @override
  $Res call({
    Object? is_custom_ebk = freezed,
    Object? klasifikasi_agen = freezed,
    Object? found = freezed,
    Object? kode_wil_bi = freezed,
    Object? username = freezed,
    Object? ssn = freezed,
    Object? ssn_2 = freezed,
    Object? status = freezed,
    Object? kode_mitra = freezed,
    Object? last_login = freezed,
    Object? alamat_jalan = freezed,
    Object? alamat_rt_perum = freezed,
    Object? alamat_kelurahan = freezed,
    Object? alamat_kecamatan = freezed,
    Object? alamat_kabupaten_kota = freezed,
    Object? alamat_propinsi = freezed,
    Object? email = freezed,
    Object? no_tlp_hp = freezed,
    Object? no_tlp_rmh = freezed,
    Object? agent_type = freezed,
    Object? kode_loket = freezed,
    Object? nama = freezed,
    Object? kode_cabang = freezed,
    Object? priv_id = freezed,
    Object? favourite = freezed,
    Object? favourite_mobile = freezed,
    Object? accountNum = freezed,
    Object? kelompok_id = freezed,
    Object? menu = freezed,
    Object? group_id = freezed,
    Object? status_mitra = freezed,
    Object? no_ktp = freezed,
    Object? agen_elpiji = freezed,
    Object? nama_usaha = freezed,
    Object? flag_force_lokasi = freezed,
  }) {
    return _then(_$_DataUser(
      is_custom_ebk: is_custom_ebk == freezed
          ? _value.is_custom_ebk
          : is_custom_ebk // ignore: cast_nullable_to_non_nullable
              as int?,
      klasifikasi_agen: klasifikasi_agen == freezed
          ? _value.klasifikasi_agen
          : klasifikasi_agen // ignore: cast_nullable_to_non_nullable
              as String?,
      found: found == freezed
          ? _value.found
          : found // ignore: cast_nullable_to_non_nullable
              as int?,
      kode_wil_bi: kode_wil_bi == freezed
          ? _value.kode_wil_bi
          : kode_wil_bi // ignore: cast_nullable_to_non_nullable
              as String?,
      username: username == freezed
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      ssn: ssn == freezed
          ? _value.ssn
          : ssn // ignore: cast_nullable_to_non_nullable
              as String?,
      ssn_2: ssn_2 == freezed
          ? _value.ssn_2
          : ssn_2 // ignore: cast_nullable_to_non_nullable
              as String?,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      kode_mitra: kode_mitra == freezed
          ? _value.kode_mitra
          : kode_mitra // ignore: cast_nullable_to_non_nullable
              as String?,
      last_login: last_login == freezed
          ? _value.last_login
          : last_login // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      alamat_jalan: alamat_jalan == freezed
          ? _value.alamat_jalan
          : alamat_jalan // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat_rt_perum: alamat_rt_perum == freezed
          ? _value.alamat_rt_perum
          : alamat_rt_perum // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat_kelurahan: alamat_kelurahan == freezed
          ? _value.alamat_kelurahan
          : alamat_kelurahan // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat_kecamatan: alamat_kecamatan == freezed
          ? _value.alamat_kecamatan
          : alamat_kecamatan // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat_kabupaten_kota: alamat_kabupaten_kota == freezed
          ? _value.alamat_kabupaten_kota
          : alamat_kabupaten_kota // ignore: cast_nullable_to_non_nullable
              as String?,
      alamat_propinsi: alamat_propinsi == freezed
          ? _value.alamat_propinsi
          : alamat_propinsi // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      no_tlp_hp: no_tlp_hp == freezed
          ? _value.no_tlp_hp
          : no_tlp_hp // ignore: cast_nullable_to_non_nullable
              as String?,
      no_tlp_rmh: no_tlp_rmh == freezed
          ? _value.no_tlp_rmh
          : no_tlp_rmh // ignore: cast_nullable_to_non_nullable
              as String?,
      agent_type: agent_type == freezed
          ? _value.agent_type
          : agent_type // ignore: cast_nullable_to_non_nullable
              as String?,
      kode_loket: kode_loket == freezed
          ? _value.kode_loket
          : kode_loket // ignore: cast_nullable_to_non_nullable
              as String?,
      nama: nama == freezed
          ? _value.nama
          : nama // ignore: cast_nullable_to_non_nullable
              as String?,
      kode_cabang: kode_cabang == freezed
          ? _value.kode_cabang
          : kode_cabang // ignore: cast_nullable_to_non_nullable
              as String?,
      priv_id: priv_id == freezed
          ? _value.priv_id
          : priv_id // ignore: cast_nullable_to_non_nullable
              as String?,
      favourite: favourite == freezed
          ? _value.favourite
          : favourite // ignore: cast_nullable_to_non_nullable
              as String?,
      favourite_mobile: favourite_mobile == freezed
          ? _value._favourite_mobile
          : favourite_mobile // ignore: cast_nullable_to_non_nullable
              as List<FavouriteMobile>?,
      accountNum: accountNum == freezed
          ? _value.accountNum
          : accountNum // ignore: cast_nullable_to_non_nullable
              as String?,
      kelompok_id: kelompok_id == freezed
          ? _value.kelompok_id
          : kelompok_id // ignore: cast_nullable_to_non_nullable
              as String?,
      menu: menu == freezed
          ? _value.menu
          : menu // ignore: cast_nullable_to_non_nullable
              as dynamic,
      group_id: group_id == freezed
          ? _value.group_id
          : group_id // ignore: cast_nullable_to_non_nullable
              as dynamic,
      status_mitra: status_mitra == freezed
          ? _value.status_mitra
          : status_mitra // ignore: cast_nullable_to_non_nullable
              as dynamic,
      no_ktp: no_ktp == freezed
          ? _value.no_ktp
          : no_ktp // ignore: cast_nullable_to_non_nullable
              as String?,
      agen_elpiji: agen_elpiji == freezed
          ? _value.agen_elpiji
          : agen_elpiji // ignore: cast_nullable_to_non_nullable
              as dynamic,
      nama_usaha: nama_usaha == freezed
          ? _value.nama_usaha
          : nama_usaha // ignore: cast_nullable_to_non_nullable
              as String?,
      flag_force_lokasi: flag_force_lokasi == freezed
          ? _value.flag_force_lokasi
          : flag_force_lokasi // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DataUser implements _DataUser {
  const _$_DataUser(
      {this.is_custom_ebk,
      this.klasifikasi_agen,
      this.found,
      this.kode_wil_bi,
      this.username,
      this.ssn,
      this.ssn_2,
      this.status,
      this.kode_mitra,
      this.last_login,
      this.alamat_jalan,
      this.alamat_rt_perum,
      this.alamat_kelurahan,
      this.alamat_kecamatan,
      this.alamat_kabupaten_kota,
      this.alamat_propinsi,
      this.email,
      this.no_tlp_hp,
      this.no_tlp_rmh,
      this.agent_type,
      this.kode_loket,
      this.nama,
      this.kode_cabang,
      this.priv_id,
      this.favourite,
      final List<FavouriteMobile>? favourite_mobile,
      this.accountNum,
      this.kelompok_id,
      this.menu,
      this.group_id,
      this.status_mitra,
      this.no_ktp,
      this.agen_elpiji,
      this.nama_usaha,
      this.flag_force_lokasi})
      : _favourite_mobile = favourite_mobile;

  factory _$_DataUser.fromJson(Map<String, dynamic> json) =>
      _$$_DataUserFromJson(json);

  @override
  final int? is_custom_ebk;
  @override
  final String? klasifikasi_agen;
  @override
  final int? found;
  @override
  final String? kode_wil_bi;
  @override
  final String? username;
  @override
  final String? ssn;
  @override
  final String? ssn_2;
  @override
  final String? status;
  @override
  final String? kode_mitra;
  @override
  final DateTime? last_login;
  @override
  final String? alamat_jalan;
  @override
  final String? alamat_rt_perum;
  @override
  final String? alamat_kelurahan;
  @override
  final String? alamat_kecamatan;
  @override
  final String? alamat_kabupaten_kota;
  @override
  final String? alamat_propinsi;
  @override
  final String? email;
  @override
  final String? no_tlp_hp;
  @override
  final String? no_tlp_rmh;
  @override
  final String? agent_type;
  @override
  final String? kode_loket;
  @override
  final String? nama;
  @override
  final String? kode_cabang;
  @override
  final String? priv_id;
  @override
  final String? favourite;
  final List<FavouriteMobile>? _favourite_mobile;
  @override
  List<FavouriteMobile>? get favourite_mobile {
    final value = _favourite_mobile;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final String? accountNum;
  @override
  final String? kelompok_id;
  @override
  final dynamic menu;
  @override
  final dynamic group_id;
  @override
  final dynamic status_mitra;
  @override
  final String? no_ktp;
  @override
  final dynamic agen_elpiji;
  @override
  final String? nama_usaha;
  @override
  final int? flag_force_lokasi;

  @override
  String toString() {
    return 'DataUser(is_custom_ebk: $is_custom_ebk, klasifikasi_agen: $klasifikasi_agen, found: $found, kode_wil_bi: $kode_wil_bi, username: $username, ssn: $ssn, ssn_2: $ssn_2, status: $status, kode_mitra: $kode_mitra, last_login: $last_login, alamat_jalan: $alamat_jalan, alamat_rt_perum: $alamat_rt_perum, alamat_kelurahan: $alamat_kelurahan, alamat_kecamatan: $alamat_kecamatan, alamat_kabupaten_kota: $alamat_kabupaten_kota, alamat_propinsi: $alamat_propinsi, email: $email, no_tlp_hp: $no_tlp_hp, no_tlp_rmh: $no_tlp_rmh, agent_type: $agent_type, kode_loket: $kode_loket, nama: $nama, kode_cabang: $kode_cabang, priv_id: $priv_id, favourite: $favourite, favourite_mobile: $favourite_mobile, accountNum: $accountNum, kelompok_id: $kelompok_id, menu: $menu, group_id: $group_id, status_mitra: $status_mitra, no_ktp: $no_ktp, agen_elpiji: $agen_elpiji, nama_usaha: $nama_usaha, flag_force_lokasi: $flag_force_lokasi)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DataUser &&
            const DeepCollectionEquality()
                .equals(other.is_custom_ebk, is_custom_ebk) &&
            const DeepCollectionEquality()
                .equals(other.klasifikasi_agen, klasifikasi_agen) &&
            const DeepCollectionEquality().equals(other.found, found) &&
            const DeepCollectionEquality()
                .equals(other.kode_wil_bi, kode_wil_bi) &&
            const DeepCollectionEquality().equals(other.username, username) &&
            const DeepCollectionEquality().equals(other.ssn, ssn) &&
            const DeepCollectionEquality().equals(other.ssn_2, ssn_2) &&
            const DeepCollectionEquality().equals(other.status, status) &&
            const DeepCollectionEquality()
                .equals(other.kode_mitra, kode_mitra) &&
            const DeepCollectionEquality()
                .equals(other.last_login, last_login) &&
            const DeepCollectionEquality()
                .equals(other.alamat_jalan, alamat_jalan) &&
            const DeepCollectionEquality()
                .equals(other.alamat_rt_perum, alamat_rt_perum) &&
            const DeepCollectionEquality()
                .equals(other.alamat_kelurahan, alamat_kelurahan) &&
            const DeepCollectionEquality()
                .equals(other.alamat_kecamatan, alamat_kecamatan) &&
            const DeepCollectionEquality()
                .equals(other.alamat_kabupaten_kota, alamat_kabupaten_kota) &&
            const DeepCollectionEquality()
                .equals(other.alamat_propinsi, alamat_propinsi) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality().equals(other.no_tlp_hp, no_tlp_hp) &&
            const DeepCollectionEquality()
                .equals(other.no_tlp_rmh, no_tlp_rmh) &&
            const DeepCollectionEquality()
                .equals(other.agent_type, agent_type) &&
            const DeepCollectionEquality()
                .equals(other.kode_loket, kode_loket) &&
            const DeepCollectionEquality().equals(other.nama, nama) &&
            const DeepCollectionEquality()
                .equals(other.kode_cabang, kode_cabang) &&
            const DeepCollectionEquality().equals(other.priv_id, priv_id) &&
            const DeepCollectionEquality().equals(other.favourite, favourite) &&
            const DeepCollectionEquality()
                .equals(other._favourite_mobile, _favourite_mobile) &&
            const DeepCollectionEquality()
                .equals(other.accountNum, accountNum) &&
            const DeepCollectionEquality()
                .equals(other.kelompok_id, kelompok_id) &&
            const DeepCollectionEquality().equals(other.menu, menu) &&
            const DeepCollectionEquality().equals(other.group_id, group_id) &&
            const DeepCollectionEquality()
                .equals(other.status_mitra, status_mitra) &&
            const DeepCollectionEquality().equals(other.no_ktp, no_ktp) &&
            const DeepCollectionEquality()
                .equals(other.agen_elpiji, agen_elpiji) &&
            const DeepCollectionEquality()
                .equals(other.nama_usaha, nama_usaha) &&
            const DeepCollectionEquality()
                .equals(other.flag_force_lokasi, flag_force_lokasi));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(is_custom_ebk),
        const DeepCollectionEquality().hash(klasifikasi_agen),
        const DeepCollectionEquality().hash(found),
        const DeepCollectionEquality().hash(kode_wil_bi),
        const DeepCollectionEquality().hash(username),
        const DeepCollectionEquality().hash(ssn),
        const DeepCollectionEquality().hash(ssn_2),
        const DeepCollectionEquality().hash(status),
        const DeepCollectionEquality().hash(kode_mitra),
        const DeepCollectionEquality().hash(last_login),
        const DeepCollectionEquality().hash(alamat_jalan),
        const DeepCollectionEquality().hash(alamat_rt_perum),
        const DeepCollectionEquality().hash(alamat_kelurahan),
        const DeepCollectionEquality().hash(alamat_kecamatan),
        const DeepCollectionEquality().hash(alamat_kabupaten_kota),
        const DeepCollectionEquality().hash(alamat_propinsi),
        const DeepCollectionEquality().hash(email),
        const DeepCollectionEquality().hash(no_tlp_hp),
        const DeepCollectionEquality().hash(no_tlp_rmh),
        const DeepCollectionEquality().hash(agent_type),
        const DeepCollectionEquality().hash(kode_loket),
        const DeepCollectionEquality().hash(nama),
        const DeepCollectionEquality().hash(kode_cabang),
        const DeepCollectionEquality().hash(priv_id),
        const DeepCollectionEquality().hash(favourite),
        const DeepCollectionEquality().hash(_favourite_mobile),
        const DeepCollectionEquality().hash(accountNum),
        const DeepCollectionEquality().hash(kelompok_id),
        const DeepCollectionEquality().hash(menu),
        const DeepCollectionEquality().hash(group_id),
        const DeepCollectionEquality().hash(status_mitra),
        const DeepCollectionEquality().hash(no_ktp),
        const DeepCollectionEquality().hash(agen_elpiji),
        const DeepCollectionEquality().hash(nama_usaha),
        const DeepCollectionEquality().hash(flag_force_lokasi)
      ]);

  @JsonKey(ignore: true)
  @override
  _$$_DataUserCopyWith<_$_DataUser> get copyWith =>
      __$$_DataUserCopyWithImpl<_$_DataUser>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DataUserToJson(
      this,
    );
  }
}

abstract class _DataUser implements DataUser {
  const factory _DataUser(
      {final int? is_custom_ebk,
      final String? klasifikasi_agen,
      final int? found,
      final String? kode_wil_bi,
      final String? username,
      final String? ssn,
      final String? ssn_2,
      final String? status,
      final String? kode_mitra,
      final DateTime? last_login,
      final String? alamat_jalan,
      final String? alamat_rt_perum,
      final String? alamat_kelurahan,
      final String? alamat_kecamatan,
      final String? alamat_kabupaten_kota,
      final String? alamat_propinsi,
      final String? email,
      final String? no_tlp_hp,
      final String? no_tlp_rmh,
      final String? agent_type,
      final String? kode_loket,
      final String? nama,
      final String? kode_cabang,
      final String? priv_id,
      final String? favourite,
      final List<FavouriteMobile>? favourite_mobile,
      final String? accountNum,
      final String? kelompok_id,
      final dynamic menu,
      final dynamic group_id,
      final dynamic status_mitra,
      final String? no_ktp,
      final dynamic agen_elpiji,
      final String? nama_usaha,
      final int? flag_force_lokasi}) = _$_DataUser;

  factory _DataUser.fromJson(Map<String, dynamic> json) = _$_DataUser.fromJson;

  @override
  int? get is_custom_ebk;
  @override
  String? get klasifikasi_agen;
  @override
  int? get found;
  @override
  String? get kode_wil_bi;
  @override
  String? get username;
  @override
  String? get ssn;
  @override
  String? get ssn_2;
  @override
  String? get status;
  @override
  String? get kode_mitra;
  @override
  DateTime? get last_login;
  @override
  String? get alamat_jalan;
  @override
  String? get alamat_rt_perum;
  @override
  String? get alamat_kelurahan;
  @override
  String? get alamat_kecamatan;
  @override
  String? get alamat_kabupaten_kota;
  @override
  String? get alamat_propinsi;
  @override
  String? get email;
  @override
  String? get no_tlp_hp;
  @override
  String? get no_tlp_rmh;
  @override
  String? get agent_type;
  @override
  String? get kode_loket;
  @override
  String? get nama;
  @override
  String? get kode_cabang;
  @override
  String? get priv_id;
  @override
  String? get favourite;
  @override
  List<FavouriteMobile>? get favourite_mobile;
  @override
  String? get accountNum;
  @override
  String? get kelompok_id;
  @override
  dynamic get menu;
  @override
  dynamic get group_id;
  @override
  dynamic get status_mitra;
  @override
  String? get no_ktp;
  @override
  dynamic get agen_elpiji;
  @override
  String? get nama_usaha;
  @override
  int? get flag_force_lokasi;
  @override
  @JsonKey(ignore: true)
  _$$_DataUserCopyWith<_$_DataUser> get copyWith =>
      throw _privateConstructorUsedError;
}

FavouriteMobileSave _$FavouriteMobileSaveFromJson(Map<String, dynamic> json) {
  return _FavouriteMobileSave.fromJson(json);
}

/// @nodoc
mixin _$FavouriteMobileSave {
  String? get namaMenu => throw _privateConstructorUsedError;
  String? get id => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FavouriteMobileSaveCopyWith<FavouriteMobileSave> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FavouriteMobileSaveCopyWith<$Res> {
  factory $FavouriteMobileSaveCopyWith(
          FavouriteMobileSave value, $Res Function(FavouriteMobileSave) then) =
      _$FavouriteMobileSaveCopyWithImpl<$Res>;
  $Res call({String? namaMenu, String? id});
}

/// @nodoc
class _$FavouriteMobileSaveCopyWithImpl<$Res>
    implements $FavouriteMobileSaveCopyWith<$Res> {
  _$FavouriteMobileSaveCopyWithImpl(this._value, this._then);

  final FavouriteMobileSave _value;
  // ignore: unused_field
  final $Res Function(FavouriteMobileSave) _then;

  @override
  $Res call({
    Object? namaMenu = freezed,
    Object? id = freezed,
  }) {
    return _then(_value.copyWith(
      namaMenu: namaMenu == freezed
          ? _value.namaMenu
          : namaMenu // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_FavouriteMobileSaveCopyWith<$Res>
    implements $FavouriteMobileSaveCopyWith<$Res> {
  factory _$$_FavouriteMobileSaveCopyWith(_$_FavouriteMobileSave value,
          $Res Function(_$_FavouriteMobileSave) then) =
      __$$_FavouriteMobileSaveCopyWithImpl<$Res>;
  @override
  $Res call({String? namaMenu, String? id});
}

/// @nodoc
class __$$_FavouriteMobileSaveCopyWithImpl<$Res>
    extends _$FavouriteMobileSaveCopyWithImpl<$Res>
    implements _$$_FavouriteMobileSaveCopyWith<$Res> {
  __$$_FavouriteMobileSaveCopyWithImpl(_$_FavouriteMobileSave _value,
      $Res Function(_$_FavouriteMobileSave) _then)
      : super(_value, (v) => _then(v as _$_FavouriteMobileSave));

  @override
  _$_FavouriteMobileSave get _value => super._value as _$_FavouriteMobileSave;

  @override
  $Res call({
    Object? namaMenu = freezed,
    Object? id = freezed,
  }) {
    return _then(_$_FavouriteMobileSave(
      namaMenu: namaMenu == freezed
          ? _value.namaMenu
          : namaMenu // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_FavouriteMobileSave implements _FavouriteMobileSave {
  const _$_FavouriteMobileSave({this.namaMenu, this.id});

  factory _$_FavouriteMobileSave.fromJson(Map<String, dynamic> json) =>
      _$$_FavouriteMobileSaveFromJson(json);

  @override
  final String? namaMenu;
  @override
  final String? id;

  @override
  String toString() {
    return 'FavouriteMobileSave(namaMenu: $namaMenu, id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FavouriteMobileSave &&
            const DeepCollectionEquality().equals(other.namaMenu, namaMenu) &&
            const DeepCollectionEquality().equals(other.id, id));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(namaMenu),
      const DeepCollectionEquality().hash(id));

  @JsonKey(ignore: true)
  @override
  _$$_FavouriteMobileSaveCopyWith<_$_FavouriteMobileSave> get copyWith =>
      __$$_FavouriteMobileSaveCopyWithImpl<_$_FavouriteMobileSave>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FavouriteMobileSaveToJson(
      this,
    );
  }
}

abstract class _FavouriteMobileSave implements FavouriteMobileSave {
  const factory _FavouriteMobileSave(
      {final String? namaMenu, final String? id}) = _$_FavouriteMobileSave;

  factory _FavouriteMobileSave.fromJson(Map<String, dynamic> json) =
      _$_FavouriteMobileSave.fromJson;

  @override
  String? get namaMenu;
  @override
  String? get id;
  @override
  @JsonKey(ignore: true)
  _$$_FavouriteMobileSaveCopyWith<_$_FavouriteMobileSave> get copyWith =>
      throw _privateConstructorUsedError;
}

FavouriteMobile _$FavouriteMobileFromJson(Map<String, dynamic> json) {
  return _FavouriteMobile.fromJson(json);
}

/// @nodoc
mixin _$FavouriteMobile {
  String? get type => throw _privateConstructorUsedError;
  String? get namaMenu => throw _privateConstructorUsedError;
  String? get id => throw _privateConstructorUsedError;
  String? get namaIcon => throw _privateConstructorUsedError;
  String? get route => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FavouriteMobileCopyWith<FavouriteMobile> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FavouriteMobileCopyWith<$Res> {
  factory $FavouriteMobileCopyWith(
          FavouriteMobile value, $Res Function(FavouriteMobile) then) =
      _$FavouriteMobileCopyWithImpl<$Res>;
  $Res call(
      {String? type,
      String? namaMenu,
      String? id,
      String? namaIcon,
      String? route});
}

/// @nodoc
class _$FavouriteMobileCopyWithImpl<$Res>
    implements $FavouriteMobileCopyWith<$Res> {
  _$FavouriteMobileCopyWithImpl(this._value, this._then);

  final FavouriteMobile _value;
  // ignore: unused_field
  final $Res Function(FavouriteMobile) _then;

  @override
  $Res call({
    Object? type = freezed,
    Object? namaMenu = freezed,
    Object? id = freezed,
    Object? namaIcon = freezed,
    Object? route = freezed,
  }) {
    return _then(_value.copyWith(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      namaMenu: namaMenu == freezed
          ? _value.namaMenu
          : namaMenu // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      namaIcon: namaIcon == freezed
          ? _value.namaIcon
          : namaIcon // ignore: cast_nullable_to_non_nullable
              as String?,
      route: route == freezed
          ? _value.route
          : route // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_FavouriteMobileCopyWith<$Res>
    implements $FavouriteMobileCopyWith<$Res> {
  factory _$$_FavouriteMobileCopyWith(
          _$_FavouriteMobile value, $Res Function(_$_FavouriteMobile) then) =
      __$$_FavouriteMobileCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? type,
      String? namaMenu,
      String? id,
      String? namaIcon,
      String? route});
}

/// @nodoc
class __$$_FavouriteMobileCopyWithImpl<$Res>
    extends _$FavouriteMobileCopyWithImpl<$Res>
    implements _$$_FavouriteMobileCopyWith<$Res> {
  __$$_FavouriteMobileCopyWithImpl(
      _$_FavouriteMobile _value, $Res Function(_$_FavouriteMobile) _then)
      : super(_value, (v) => _then(v as _$_FavouriteMobile));

  @override
  _$_FavouriteMobile get _value => super._value as _$_FavouriteMobile;

  @override
  $Res call({
    Object? type = freezed,
    Object? namaMenu = freezed,
    Object? id = freezed,
    Object? namaIcon = freezed,
    Object? route = freezed,
  }) {
    return _then(_$_FavouriteMobile(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      namaMenu: namaMenu == freezed
          ? _value.namaMenu
          : namaMenu // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      namaIcon: namaIcon == freezed
          ? _value.namaIcon
          : namaIcon // ignore: cast_nullable_to_non_nullable
              as String?,
      route: route == freezed
          ? _value.route
          : route // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_FavouriteMobile implements _FavouriteMobile {
  const _$_FavouriteMobile(
      {this.type, this.namaMenu, this.id, this.namaIcon, this.route});

  factory _$_FavouriteMobile.fromJson(Map<String, dynamic> json) =>
      _$$_FavouriteMobileFromJson(json);

  @override
  final String? type;
  @override
  final String? namaMenu;
  @override
  final String? id;
  @override
  final String? namaIcon;
  @override
  final String? route;

  @override
  String toString() {
    return 'FavouriteMobile(type: $type, namaMenu: $namaMenu, id: $id, namaIcon: $namaIcon, route: $route)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FavouriteMobile &&
            const DeepCollectionEquality().equals(other.type, type) &&
            const DeepCollectionEquality().equals(other.namaMenu, namaMenu) &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.namaIcon, namaIcon) &&
            const DeepCollectionEquality().equals(other.route, route));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(type),
      const DeepCollectionEquality().hash(namaMenu),
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(namaIcon),
      const DeepCollectionEquality().hash(route));

  @JsonKey(ignore: true)
  @override
  _$$_FavouriteMobileCopyWith<_$_FavouriteMobile> get copyWith =>
      __$$_FavouriteMobileCopyWithImpl<_$_FavouriteMobile>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FavouriteMobileToJson(
      this,
    );
  }
}

abstract class _FavouriteMobile implements FavouriteMobile {
  const factory _FavouriteMobile(
      {final String? type,
      final String? namaMenu,
      final String? id,
      final String? namaIcon,
      final String? route}) = _$_FavouriteMobile;

  factory _FavouriteMobile.fromJson(Map<String, dynamic> json) =
      _$_FavouriteMobile.fromJson;

  @override
  String? get type;
  @override
  String? get namaMenu;
  @override
  String? get id;
  @override
  String? get namaIcon;
  @override
  String? get route;
  @override
  @JsonKey(ignore: true)
  _$$_FavouriteMobileCopyWith<_$_FavouriteMobile> get copyWith =>
      throw _privateConstructorUsedError;
}
