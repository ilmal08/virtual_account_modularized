// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/config/route_string_utils.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/example_model_arguments.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/presentation/components/no_route.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/presentation/views/favorit_biller/views/favorit_biller_screen.dart';

class Routes {
  Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case favorit_biller:
        return MaterialPageRoute(
          builder: (_) => FavoritBiller(
              // arguments: args,
              ),
        );

      // if route not added
      default:
        // return null;
        return MaterialPageRoute(
          builder: (_) => NoRouteComponent(),
        );
    }
  }
}
