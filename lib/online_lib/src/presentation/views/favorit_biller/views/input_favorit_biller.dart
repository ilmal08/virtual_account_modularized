// ignore_for_file: public_member_api_docs, sort_constructors_first, prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/example_model_arguments.dart';

class InputFavoritBiller extends StatefulWidget {
  // final FavoritBillingArguments? arguments;
  InputFavoritBiller({
    Key? key,
    // this.arguments,
  }) : super(key: key);

  @override
  State<InputFavoritBiller> createState() => _InputFavoritBillerState();
}

class _InputFavoritBillerState extends State<InputFavoritBiller> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Input Favorit Billing'),
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            print('back is pressed from online modul input biller');
            // Navigator.of(context).popUntil((route) => route.isFirst);
            // SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            // Navigator.of(context, rootNavigator: false).pop();
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back),
        ),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            Text(
              'Ini adalah halaman INPUT FAVORIT BILLER akun yang telah di modularisasi di online repository',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 22,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
