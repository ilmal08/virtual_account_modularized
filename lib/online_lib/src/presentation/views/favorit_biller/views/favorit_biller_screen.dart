// ignore_for_file: public_member_api_docs, sort_constructors_first, prefer_const_constructors, avoid_print
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/model/auth_model/login_freezed_model.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/presentation/views/favorit_biller/bloc/favorit_billing_bloc.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/presentation/views/favorit_biller/bloc/favorit_billing_state.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/presentation/views/favorit_biller/views/input_favorit_biller.dart';

class FavoritBiller extends StatefulWidget {
  final LoginFreezedModel? model;
  FavoritBiller({
    Key? key,
    this.model,
  }) : super(key: key);

  @override
  State<FavoritBiller> createState() => _FavoritBillerState();
}

class _FavoritBillerState extends State<FavoritBiller> {
  late FavoritBillingBloc _favoritBillingBloc;
  @override
  void initState() {
    print('start favorit biller screen from online repo');
    _favoritBillingBloc = BlocProvider.of<FavoritBillingBloc>(context);
    _favoritBillingBloc.initFavoritScreen();
    _favoritBillingBloc.fetchListFavoritBilling();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<FavoritBillingBloc, FavoritBillingState>(
      bloc: _favoritBillingBloc,
      listener: (context, state) {
        if (state is FavoritBillingLoading) {
          print('show loading di online repo');
        }

        if (state is FavoritBillingLoaded) {
          print('data di online repo berhasil diambil');
        }

        if (state is FavoritBillingError) {
          print('show dialog error di online repo');
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Favorit Billing'),
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              print('back is pressed from online modul');
              // Navigator.of(context).popUntil((route) => route.isFirst);
              // SystemChannels.platform.invokeMethod('SystemNavigator.pop');
              Navigator.of(context, rootNavigator: true).pop();
            },
            child: Icon(Icons.arrow_back),
          ),
        ),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              // Text(
              //   'Ini adalah halaman virtual akun yang telah di modularisasi di online repository\n${widget.stringData}',
              //   style: TextStyle(
              //     fontWeight: FontWeight.bold,
              //     fontSize: 17,
              //   ),
              // ),
              Text(
                'tokken dari main apps\n${widget.model?.session}',
                style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                ),
              ),
              Text(
                'nama dari main apps\n${widget.model?.dataUser?.nama}',
                style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                ),
              ),
              Text(
                'no rekening dari main apps\n${widget.model?.dataUser?.accountNum}',
                style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                ),
              ),
              SizedBox(height: 30),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => InputFavoritBiller(),
                    ),
                  );
                },
                child: Text(
                  'ke halaman input biller',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                  ),
                ),
              ),
              BlocBuilder<FavoritBillingBloc, FavoritBillingState>(
                  bloc: _favoritBillingBloc,
                  builder: (context, state) {
                    if (state is FavoritBillingLoaded) {
                      return ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: state.loginFreezedModel.data?.length,
                        itemBuilder: (BuildContext context, int index) {
                          var data = state.loginFreezedModel.data?[index];
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 15),
                            child: Container(
                              color: Colors.grey[200],
                              width: MediaQuery.of(context).size.width,
                              height: 50,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    data?.favorite_name ?? '',
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  ),
                                  Text(
                                    data?.favorite_billing ?? '',
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    }
                    if (state is FavoritBillingError) {
                      return Text(
                        'Error Fetch Data\nTapi BLOC SUKSES DIEKSEKUSI DI ONLINE REPO',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                        ),
                      );
                    }
                    return Text(
                      'Belum Build apa2',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                      ),
                    );
                  }),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}
