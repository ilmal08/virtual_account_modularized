// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/local/hive_string_extension.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/model/auth_model/login_freezed_model.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/model/failure_global_entitiy.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/model/favourite_billing_model/favorit_billing_freezed_model.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/repository/favourite_billing_repository/favourite_billing_repository.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/service/favourite_billing_service.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/presentation/views/favorit_biller/bloc/favorit_billing_state.dart';
import 'package:path_provider/path_provider.dart';

class FavoritBillingBloc extends Cubit<FavoritBillingState> {
  FavoritBillingBloc() : super(FavoritBillingInitial());

  FavouriteBillingRepository? repository = FavoritBillingService();

  void initFavoritScreen() {
    print("bloc separate funct");
    print("from favorit billing screen online repo");
  }

  Future<void> fetchListFavoritBilling() async {
    emit(FavoritBillingLoading());

    Either<Failure, FavouriteBillingFreezedModel> result =
        await repository!.getFavouriteBillingService(
      kodeTransaksi: '402',
    );

    FavoritBillingState stateResult = result.fold(
      (failure) {
        print(failure);
        RequestFailure f = failure as RequestFailure;
        return FavoritBillingError(code: f.code, message: f.message);
      },
      (data) {
        return FavoritBillingLoaded(
          loginFreezedModel: data,
        );
      },
    );
    // log.wtf(stateResult);
    emit(stateResult);
  }
}
