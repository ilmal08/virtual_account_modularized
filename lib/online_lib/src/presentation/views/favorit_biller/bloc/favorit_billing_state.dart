import 'package:equatable/equatable.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/model/favourite_billing_model/favorit_billing_freezed_model.dart';

class FavoritBillingState extends Equatable {
  @override
  List<Object?> get props => [];
}

class FavoritBillingInitial extends FavoritBillingState {}

class FavoritBillingLoading extends FavoritBillingState {}

class FavoritBillingLoaded extends FavoritBillingState {
  final FavouriteBillingFreezedModel loginFreezedModel;

  FavoritBillingLoaded({required this.loginFreezedModel});

  @override
  List<Object> get props => [loginFreezedModel];
}

class FavoritBillingError extends FavoritBillingState {
  final int? code;
  final String? message;

  FavoritBillingError({
    required this.code,
    required this.message,
  });

  @override
  List<Object?> get props => [code, message];
}
