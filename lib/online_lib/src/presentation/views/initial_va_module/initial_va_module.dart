// ignore_for_file: public_member_api_docs, sort_constructors_first, prefer_const_constructors
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/local/hive_string_extension.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/model/auth_model/login_freezed_model.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/presentation/views/favorit_biller/bloc/favorit_billing_bloc.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/utils/dependency_injector.dart'
    as dependency_injector;
import 'package:modularized_virtual_account_rnd/virtual_account_modularized.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class InitialVaModule extends StatefulWidget {
  final String? stringArgmument;
  InitialVaModule({
    Key? key,
    this.stringArgmument,
  }) : super(key: key);

  @override
  State<InitialVaModule> createState() => _InitialVaModuleState();
}

class _InitialVaModuleState extends State<InitialVaModule> {
  // final favoritBloc = dependency_injector.sl<FavoritBillingBloc>();

  var box = Hive.box(HiveString.localStorage);
  void hiveInit() async {
    // initialized hive
    var path = Directory.current.path;
    Hive.init((await getApplicationDocumentsDirectory()).path);
    await Hive.openBox(HiveString.localStorage);
  }

  LoginFreezedModel loginFreezedModel = LoginFreezedModel();
  void getAuthData() async {
    var jsonString = box.get(HiveString.authData);
    loginFreezedModel = LoginFreezedModel.fromJson(jsonDecode(jsonString));
    box.put(HiveString.session_usr, loginFreezedModel.session ?? '');
    box.put(
        HiveString.user_name_usr, loginFreezedModel.dataUser?.username ?? '');
    box.put(HiveString.nama_usr, loginFreezedModel.dataUser?.nama ?? '');
    box.put(HiveString.nama_usaha_usr,
        loginFreezedModel.dataUser?.nama_usaha ?? '');
    box.put(HiveString.account_num_usr,
        loginFreezedModel.dataUser?.accountNum ?? '');
    box.put(HiveString.kode_mitra_usr,
        loginFreezedModel.dataUser?.kode_mitra ?? '');
    box.put(HiveString.kode_cabang_usr,
        loginFreezedModel.dataUser?.kode_cabang ?? '');
    box.put(HiveString.kode_loket_usr,
        loginFreezedModel.dataUser?.kode_loket ?? '');
    box.put(HiveString.alamat_jalan_usr,
        loginFreezedModel.dataUser?.alamat_jalan ?? '');
    box.put(HiveString.alamat_perum_usr,
        loginFreezedModel.dataUser?.alamat_rt_perum ?? '');
    box.put(HiveString.alamat_kelurahan_usr,
        loginFreezedModel.dataUser?.alamat_kelurahan ?? '');
    box.put(HiveString.alamat_kecamatan_usr,
        loginFreezedModel.dataUser?.alamat_kecamatan ?? '');
    box.put(HiveString.alamat_kabupaten_usr,
        loginFreezedModel.dataUser?.alamat_kabupaten_kota ?? '');
    box.put(HiveString.alamat_provinsi_usr,
        loginFreezedModel.dataUser?.alamat_propinsi ?? '');
    box.put(HiveString.email_usr, loginFreezedModel.dataUser?.email ?? '');
    box.put(HiveString.no_hp_usr, loginFreezedModel.dataUser?.no_tlp_hp ?? '');
    box.put(HiveString.no_telepon_usr,
        loginFreezedModel.dataUser?.no_tlp_rmh ?? '');
    box.put(HiveString.kelompok_id_usr,
        loginFreezedModel.dataUser?.kelompok_id ?? '');
    box.put(HiveString.no_ktp_usr, loginFreezedModel.dataUser?.no_ktp ?? '');
    box.put(
        HiveString.agen_lpg_usr, loginFreezedModel.dataUser?.agen_elpiji ?? '');
  }

  @override
  void initState() {
    box.put(HiveString.authData, widget.stringArgmument);
    hiveInit();
    getAuthData();
    // navigasi
    // Future.delayed(Duration(seconds: 1), () {
    //   Navigator.pushReplacement(
    //     context,
    //     MaterialPageRoute(
    //       builder: (context) => FavoritBiller(
    //           // arguments: authData,
    //           ),
    //     ),
    //   );
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        BlocProvider<FavoritBillingBloc>(
          create: (context) => FavoritBillingBloc(),
        ),

        // BlocProvider(
        //   create: (_) => favoritBloc,
        // ),
      ],
      child: FavoritBiller(
        model: loginFreezedModel,
      ),
    );
  }
}
