// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class NoRouteComponent extends StatelessWidget {
  const NoRouteComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('rute tidak ditemukan'),
      ),
    );
  }
}
