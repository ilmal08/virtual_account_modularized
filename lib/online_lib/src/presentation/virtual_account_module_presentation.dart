import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/config/route_string_utils.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/presentation/routes/virtual_accounts_routes.dart';
import 'package:provider/provider.dart';

class VirtualAccountModulePresentation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() =>
      _VirtualAccountModulePresentationState();
}

class _VirtualAccountModulePresentationState
    extends State<VirtualAccountModulePresentation> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  NavigatorState get _navigator => _navigatorKey.currentState!;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // SV.toKey("PSCIQGfoZidjEuWtJAdn1JGYzKDonk9YblI0uv96O8s=");
    final textTheme = Theme.of(context).textTheme;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // navigatorObservers: [ChuckerFlutter.navigatorObserver],
      navigatorKey: _navigatorKey,
      initialRoute: favorit_biller,
      onGenerateRoute: Routes().onGenerateRoute,
    );
  }
}
