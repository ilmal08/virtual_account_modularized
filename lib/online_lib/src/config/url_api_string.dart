class UriApi {
  UriApi._();
  /*----------------------------BASE API-----------------------------------*/
  static const String baseApi = "https://api.agen46.id";

  /*----------------------VA BILLING API-----------------------------------*/
  static const String virtualAccountBillingInquiryUrl = "/vaBillingInquiry";

  static const String virtualAccountBillingPaymentUrl = "/vaBillingPayment";

  static const String setFavouriteBilling = "/setFavoritBilling";

  static const String getFavouriteBilling = "/getFavoritBillingList";
}
