// ignore_for_file: avoid_print

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class DioLogingIntercepotrs extends InterceptorsWrapper {
  // ignore: unused_field

  // final GlobalKey<NavigatorState> navigatorKey =
  //     new GlobalKey<NavigatorState>();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    print('REQUEST[${options.method}] => PATH: ${options.path}');
    print('REQUEST[${options.method}] => HEADER: ${options.headers}');
    print('REQUEST[${options.method}] => DATA: ${options.data}');
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    print(
      'RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}',
    );
    print(
      'RESPONSE[${response.data}] => PATH: ${response.requestOptions.path}',
    );
    // if (response.statusCode == 401 ||
    //     response.statusCode == null ||
    //     response.data
    //         .toString()
    //         .contains(RegExp('code : 66', caseSensitive: true))) {
    //   print("Remove Session onResponse");
    //   sessionExpiredAllertDialog(
    //     NavigationService.navigatorKey.currentContext,
    //     [TextSpan(text: '')],
    //     true,
    //     () {
    //       Navigator.of(NavigationService.navigatorKey.currentContext)
    //           .pushNamedAndRemoveUntil(loginScreen, (route) => false);
    //     },
    //   );
    // }
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    print(
      'ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path}',
    );
    // if (err.response?.statusCode == 401 ||
    //     err.response?.statusCode == null ||
    //     err.response!.data
    //         .toString()
    //         .contains(RegExp('code : 66', caseSensitive: true))) {
    //   print("Remove Session on error dio");
    //   sessionExpiredAllertDialog(
    //     NavigationService.navigatorKey.currentContext,
    //     [TextSpan(text: '')],
    //     true,
    //     () {
    //       Navigator.of(NavigationService.navigatorKey.currentContext)
    //           .pushNamedAndRemoveUntil(loginScreen, (route) => false);
    //     },
    //   );
    // }
    return super.onError(err, handler);
  }
}
