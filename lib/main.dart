// ignore_for_file: depend_on_referenced_packages
// commend all on for production
// uncommend all code here if you want to debug module
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/data/local/hive_string_extension.dart';
import 'package:modularized_virtual_account_rnd/online_lib/src/utils/dependency_injector.dart'
    as dependency_injector;
import 'package:modularized_virtual_account_rnd/online_lib/virtual_account_app.dart';
import 'package:path_provider/path_provider.dart';

void main() async {
  // dependency_injector.init();

  // ensure initialized
  WidgetsFlutterBinding.ensureInitialized();

  // initialized hive
  var path = Directory.current.path;
  Hive.init((await getApplicationDocumentsDirectory()).path);
  await Hive.openBox(HiveString.localStorage);

  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));

  try {
    runApp(
      VirtualAccountApp(),
    );
  } catch (e) {
    debugPrint("Error" + e.toString());
  }
}
